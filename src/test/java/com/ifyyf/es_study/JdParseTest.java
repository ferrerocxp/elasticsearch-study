package com.ifyyf.es_study;

import com.ifyyf.es_study.pojo.Content;
import com.ifyyf.es_study.service.ContentService;
import com.ifyyf.es_study.utils.HtmlParseUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-10-14 下午 08:26
 */
@SpringBootTest
public class JdParseTest {

    @Autowired
    private ContentService contentService;

    @Test
    void searchJd() throws IOException {
        List<Content> java = HtmlParseUtil.searchJd("java");
        for (Content content : java) {
            System.out.println(content);
        }
    }

    @Test
    void batchInsertFromJd() throws IOException {
        System.out.println(contentService.batchInsertFromJd("java"));
    }

    @Test
    void searchByKeyWord() throws IOException {
        List<Map<String, Object>> list = contentService.searchByKeyWord("编程思想",1,3);
        for (Map<String, Object> map : list) {
            System.out.println(map);
        }
    }
    @Test
    void searchHighLight() throws IOException {
        List<Map<String, Object>> list = contentService.searchHighLight("java", 1, 10);
        for (Map<String, Object> map : list) {
            System.out.println(map);
        }
    }
}
