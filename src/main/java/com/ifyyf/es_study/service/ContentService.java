package com.ifyyf.es_study.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-10-14 下午 08:20
 */
public interface ContentService {
    boolean batchInsertFromJd(String key) throws IOException;
    List<Map<String,Object>> searchByKeyWord(String key,int from,int size) throws IOException;
    List<Map<String,Object>> searchHighLight(String key,int from,int size) throws IOException;
}
