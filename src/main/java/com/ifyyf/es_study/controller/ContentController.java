package com.ifyyf.es_study.controller;

import com.ifyyf.es_study.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-10-14 下午 08:19
 */
@RestController
public class ContentController {

    @Autowired
    private ContentService contentService;

    @GetMapping("/searchByKeyWord/{key}/{page}/{size}")
    public List<Map<String,Object>> searchByKeyWord(@PathVariable String key,
                                                    @PathVariable int page,
                                                    @PathVariable int size) throws IOException {
        return contentService.searchByKeyWord(key,page,size);
    }
    @GetMapping("/searchHighLight/{key}/{page}/{size}")
    public List<Map<String,Object>> searchHighLight(@PathVariable String key,
                                                    @PathVariable int page,
                                                    @PathVariable int size) throws IOException {
        return contentService.searchHighLight(key,page,size);
    }
}
