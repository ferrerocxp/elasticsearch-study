package com.ifyyf.es_study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(EsStudyApplication.class, args);
    }

}
