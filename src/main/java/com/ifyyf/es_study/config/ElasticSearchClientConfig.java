package com.ifyyf.es_study.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author if
 * @Description: 配置client对象，注册到spring的ioc容器中
 * @Date 2021-10-07 下午 06:04
 */
@Configuration //如果是spring项目，相当于配置xml的bean
public class ElasticSearchClientConfig {
    @Bean //<beans id="restHighLevelClient" class="RestHighLevelClient">
    public RestHighLevelClient restHighLevelClient(){
        RestHighLevelClient restHighLevelClient=new RestHighLevelClient(
                RestClient.builder(
                        //如果是集群就可以构建多个,学习时构建单个即可
                        new HttpHost("127.0.0.1",9200,"http")));
        return restHighLevelClient;
    }
}
