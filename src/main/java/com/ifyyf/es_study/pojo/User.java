package com.ifyyf.es_study.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-10-11 下午 05:49
 */
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String name;
    private Integer age;
}
