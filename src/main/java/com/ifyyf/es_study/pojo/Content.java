package com.ifyyf.es_study.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Author if
 * @Description: What is it
 * @Date 2021-10-14 下午 08:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Content {
    private String title;
    private String img;
    private String price;
}
